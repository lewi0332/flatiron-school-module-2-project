# Module 2 Project
Flatiron School - NYC

---
## Data Science Immersive - Jan 28th, 2019 Cohort
---
## Contributors - [Marc Schneider](https://github.com/MarcSchneider71), [Rene Ventura](https://github.com/rvent) and [Derrick Lewis](https://github.com/lewi0332)
---

The requirements of this project are to collect data, store them in either a Pandas dataframe or database, conduct exploratory analysis, and conduct hypothesis testing using the linear models.

Marc Schneider, Rene Ventura and Derrick Lewis collaborated on this project. 

# True Reach Estimator

We have been asked to build a predictive model that will estimate the reach of an Instagram post. This data is accessible through the business API, but in Q1 2018 Instagram blocked marketing teams from this endpoint. We are able to collect a sample of this data to see if the are working correlations between current public data and the true reach/impressions that were previously available.

Data: the CSV that we have, contains one year of data from 5,000 Influencer Level Instagram users. Influencer level indicates that these users are intending their channel to be viewed as a valuable content medium for advertisers.  

### The Project Goal:

Given the now lack of reach and impression data points, create an accurate estimate of reach and impressions using only the now public information of number of followers, number of likes, and number of comments. The ideal will be a prediction with an accuracy within 10%.

### Preliminary Effort

To get started we need to import data from a csv file and add column headers to the file. For this model we will use the pandas library in python to take advantage of the built-in methods of cleaning data and building a regression model. After creating a DataFrame object in pandas, we examine the data to determine how a model could be built. We first look to see if our variables have any usable correlation to the reach variable. Then we test to see if these variables are too correlated to each other. 

### Multicollinearity Of Features
Next we look at full correlation. The test here is to make sure that any two of our variables aren't so correlated that they account for the same effect on the dependent variable.

1. Create scatter plots of each independent variable against the dependent variable, reach, to see if there is an obvious correlation in the data. 

![likes to reach correlation ](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_likes_correlation.png?raw=true)
![followers to reach correlation](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_followers_correlation.png?raw=true)
![comments to reach correlation](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_comments_correlation.png?raw=true)

These plots allow us to see that there are outliers and noise in our data. We will use this to set conditions on the data to eliminate outliers.

2. Create a correlation matrix to check how our independent variables are correlated to each other. Should any two variables be too directly correlated, we will need to exclude it. 

![correlation test of features](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_feature_correlation.png?raw=true)

![heat map of correlation of features](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_initial_feature_correlation.png?raw=true)

Given this view we can see that none of our features are more correlated than .75. Which is to say that all of our features are acceptable to use. 

3. Interactions

![Scatter plot of our three features](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_interactions.png?raw=true)

--- 

## Trial 1 - Minimal cleaning of outliers. 

In this first pass, we will perform some minimal cleaning of the data to remove outliers and attempt to fit a multi-variable regression line. 

* We attempt to clean the data by removing values of zero and extremely large occurrences
* Create a regression model on our variables to determine the effectiveness of the data so far
* Use the regression model to check for accuracy in the prediction
* Use the regression model to plot an interaction chart to see if our features will work together
* Summarize our findings and suggest next steps if unsucessful


Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| .507| Intercept  | -1380.14 | 0 |  
| | followers | 0.0033 | 0 |
| |  likes |  9.4501 | 0 |
| |  comments | -6.1339 | 0|

We used our new linear regression model to calculate a predicted reach for our data. Below we have a histogram plot showing the accuracy of the results. 

![prediction histogram for initial trial](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/01_predict_hist.png?raw=true)

**Summary of Trial 1** - Initial Trial 

We did a minimal amount of cleaning and saw some decent accuracy, but our Root Mean Squared Error was dramatically off. Additionally, all values near the low end will produce a negative value as the intercept is far below zero. Intuitively it is impossible for a user with 500 followers to have negative reach. We believe our data might need to be more normally distributed. To try this we will perform a logarithmic transformation of the values before testing the model. 

## Trial 2 - Logarithmic Transformation of Values. 

After an initial trial of exploring the data and cleaning it up. We decided to do a log transformation and scale the data.

Process:
* Import the data
* Split the data into a test and training set
* Observe the data once again before doing a transformation
* Log transform the training set
* Scale the data after the log transformation
* Create a regression model on our variables to determine the effectiveness of the data so far
* Use the regression model to check for accuracy in the prediction across the dataset
* Summarize our findings and suggest next steps if unsuccessful


Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| 0.223| Intercept  | 10200.00 | 0 |  
| | followers | 1952.3737 | 0 |
| |  likes |  13320.0 | 0 |
| |  comments | -1029.0807 | 0|

**Summary of Trial 2** - Log Transformation:

Even after doing a log transform the R^2 was just above 21% and the RMSE was over 27,000 on our training data. For our test data, the RMSE was over 100,000(log transform) on the first model with our test dataset and over 200,000 on our second model(log transform and scaled). We conclude that we must do some further analysis on the data. In our next trial, we will see the effects of a curvilinear regression.


## Trial 3 - Polynomial Regression on Comments Feature

In this trial, we looked to apply a curvilinear regression to our Comments variable, as this feature has seemed to create some inaccurate results in a linear regression format. 

To do this we will match it with a polynomial regression and use this to adjust the model accordingly. 

Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| .0.508 | Intercept  | -1264.0783 | 0 |  
| | followers | 0.0025 | 0.004 |
| |  likes |  9.5778 | 0 |
| |  comments | -12.1064 | 0|
| |  comments^2 | 0.0025 | 0|

We used our new linear regression model to calculate a predicted reach for our data. Below we have a histogram plot showing the accuracy of the results. 

![curvilinear accuracy histogram](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/03_predict_hist.png?raw=true)


**Summary of Trial 3** - Curvilinear Trial:

This trial produced less than ideal R^2 results and poor coefficients.  

## Trial 4  - Group-by 

In this trial, we will first normalize each users data to their central tendencies before creating coefficiencies.
Given that our earlier trials are producing varied results, we will attempt to group each user to a central tendency first. As Instagram's algorithm might produce highly varied results even among the same user, we believe this process will help normalize against Instagrams algorithm and better predict an accurate reach.

Process:
* Import the CSV
* Clean data
* Group rows by user and aggregate by mean
* View scatter plot of the data to check for better correlation
* Create model on new data
* Test accuracy of the model against the training data

Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| 0.661 | Intercept  | -843.8577 | 0 |  
| | followers | 0.0121 | 0.078 |
| |  likes |  7.9537 | 0 |
| |  comments | -7.3598 | 0.002 |

We used our new linear regression model to calculate a predicted reach for our data. Below we have a histogram plot showing the accuracy of the results. 

![04 group by prediction histogram](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/04_predict_hist.png?raw=true)

**Summary of Trial 4** - Group-By Trial:

In this trial, we found the mean of each users data first before determining the coefficients of the model. This lowered the Root Mean Squared Error in our accuracy ratio versus an ungrouped set. However, our R-Squared has gone down. This Group-By trial had decent results but still not the accuracy we were hoping for. The plot of our correlation of followers to reach creates a view that doesn't suggest a correlation. 


## Trial 5 - Fewer features 

In this trial, we posit that our Comments field may be costing our model accuracy. Thus, we create a model with just two features to see if we can more accurately predict the reach. 

Process: 

*Use only followers and likes as features in our regression. 

Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| 0.512| Intercept  | -769.3995 | 0 |  
| | followers | 0.0587 | 0 |
| |  likes |  6.5412 | 0 |

**Summary of Trial 5:** - Fewer Features:

After running our analysis excluding comments in our independent variables, we determine that this is not our best fitting model based on our R^2 and mean squared error value. We still do not have a model that satisfies our goal.
In the next trial we will reintroduce the Comments as a feature, but perform some feature engineering on the data.

## Trial 6 - Label Encoding of the Comments Feature

In this trial, we distribute the comment values into categorical bins. 

In previous trials, the coefficient of our 'comments' feature was often negative. Additionally, when visualizing a scatter plot of our comments versus reach we did not see an obvious correlation. The data points were clustered in a curve near the x-y axis.

Label encoding will allow us to assign bins to the data and weight them appropriately. We will use this to transform the coefficient and potentially help our regression become less sensitive to the 'comments' field.
To begin, we will use data that has already been grouped by user. There is a single row for each instagram user and the data that follows is the mean of the combined values.

![Bins](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/06_bins.png?raw=true)

![correlation of comments after bins](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/06_coor_bins.png?raw=true)

Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| 0.735| Intercept  | -153.24 | 0.700 |  
| | followers | 0.0332 | 0 |
| | likes | 6.3817 | 0 |
| | comments [1] | -36.5232 | 0.948 |
| | comments [2] | 103.3595 | 0.855 |
| | comments [3] | 53.99 | 0.929 |

We used our new linear regression model to calculate a predicted reach for our data. Below we have a histogram plot showing the accuracy of the results. 

![Prediction with label encoding](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/06_predict_hist.png?raw=true)

**Summary of Trial 6** - Label Encoding

Label encoding of our comments feature created a scatterplot that seemed to be able to better add to our coefficients. However, our R-Squared remained near .73, which is great, but not better than others and our Root Mean Square Error of this trial is very large at 11,900. 

--- 

# Once Again, from the Beginning. 

Until now we have not had great success with our model predicting our dependent variable. So, we started again from the beginning with a deeper data exploration.  

**Further exploration:**
* Outliers are more complicated than previously imagined. In removing data that was either very large or very small we may have been removing points that fell very accurately in our prediction.  What we discovered in a deeper exploration is that the real outliers are items that violate a ratio of variables. 
* Having 2,000 comments on a post is not strange if that post is from a user with 250,000 followers. However if a person with only 20 followers had that same amount of comments, that would violate normality. 
* Understanding how Instagram's platform and algorithm works allows for these very unnatural results. Additionally, for this model we are not looking to account for these types of occurrences. 

**Remove outliers by Ratio:**
* Understand that this technique may not be ideal, but in this instance our data is not naturally occurring. Instagram's algorithm artificially suppresses or boosts posts skewing our input data.  
* Test for combinations of variable ratios that would not be logical (1mil followers, 10 likes). 
* Remove ratios in a manner that is symmetrical and uniform from all three features to maintain even variability of the remaining data.

# Final Trial - Remove outliers using a relational measure

After many trials we have learned that while our 3 variables are appropriate for a regression we were still not able to accurately predict the dependent variable.

In this trial we focused on exploring the data looking, but we applied more domain knowledge to the process to find the most appropriate method to remove outlier data. These data points had a detrimental effect on any prediction results. Simply eliminating outliers of a single large or small single variable has a negative effect on our model as the scale of our users varies greatly. A user might have 10,000 comments, which could seem like an outlier, until you also find they 1 million followers. What is instead a non-normal event is finding a user with the same 1 million followers and only 10 comments.

We determined that issues arose when the ratios of certain variables violated normality. As such, we chose to eliminate the upper and lower 10% of the offending ratios to help fit the model. We did this symetrically and uniformally across the three variables to maintain a set that could still have some unbiased relationship.
For this group we have only included users with followers from 1 to 10,000 to better match the reactions to posts.

Process:

work with users that have 0-10k followers.
Change the confidence interval to 10% or 90%.

![08 follower corr](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/08_fol_corr.png?raw=true)

![liks corr](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/08_likes_corr.png?raw=true)

![08 comments corr](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/08_com_corr.png?raw=true)

Ordinary Least Squares summary table: 

| R^2 |  Feature | Coefficient | P-Value |
|---|---|---|---|
| 0.749| Intercept  | 40.90 | 0 |  
| | followers | 0.0898 | 0 |
| |  likes |  2.867 | 0 |
| |  comments | 6.2121 | 0|

- Here is a histogram view of the accuracy of this latest model against the training data which has had the outliers removed. 

We used our linear regression model to calculate a predicted reach for our data. Below we have a histogram plot showing the accuracy of the results against just the training data. 

![accuracy of model against training data](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/08_predict_train.png?raw=true)

- Finally this model seemed good enough to test our accuracy method against an unedited test set of data which was removed before our model was fit:  

![accuracy of model against test set](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/graph_images/08_predict_test.png?raw=true)

**Summary of eliminating outliers by relationship:**
This method has given us the most accurate results, abeit obvious as we have stronghanded the outlier data. We have decided to move forward with this process of removing outliers by ratio to reach. We will keep the cuts symetric and unifrom.

You can see our prediction model performed with decent accuracy against an edited data set. 

### Grouping user by influence.

Next we take this process one step further by grouping our users by their follower count. Users that have small following will have a different reactions in performance. Thus as our group size is reduced we can more accurately predict reach for that group.

As a simple test we broke our data into 5 groups with equal volume of variables. However, this creates wider ranges of users as the follower count grows. Meaning there are as many accounts between 1-5,000 as there are between 25,000-65,000.  The difference in range creates slightly different accuracy between groups. If we had more data we could set groups at uniform range. 

[Final Reach Estimate 1 - 5,000 Followers](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/reach_est_1_5k.ipynb)

[Final Reach Estimate 5,000 - 12,500 Followers](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/reach_est_5k_12.5k.ipynb)

[Final Reach Estimate 12,500 - 25,000 Followers](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/reach_est_12.5k_25k.ipynb)

[Final Reach Estimate 25,000 - 65,000 Followers](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/reach_est_25k_65k.ipynb)

[Final Reach Estimate 65,000 and Greater Followers ](https://github.com/MarcSchneider71/SocialMediaReach/blob/master/reach_est_65k_greater.ipynb)

## Results: 
- We have produced a model that might be useful. However it does not meet the accuracy requirements.
- This model will certainly need to be used with caveat of understanding that the client inputing obvious outlier cases will not garner a normal prediction. 

## Future work: 
Going forward this tool could gain further accuracy with more features. Instagrams algorithm and the natural habits of certain demographics of age, gender, geo-region and many other variables could be used to gain accuracy
